const express = require('express');
const path = require('path');
const https = require('https');
const bodyParser = require('body-parser');
const fs = require("fs");
const { json } = require('body-parser');

const app = express()
const port = 3000;
const PATH_POSTS = "./db/user.json";

app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({extended: false}))
app.use(express.static(__dirname + '/public'));

app.get("/", (req, res) => {
  res.render("index")
})

app.post("/login", (req, res) => {
  res.redirect("/login");
})

app.get("/login", (req, res) => {
  res.render("login")
})

app.get("/game", (req, res) => {
  res.render("game")
})

app.post("/game", (req, res) => {
  let nameInput = req.body.nickname
  let emailInput = req.body.email
  if(!nameInput || !emailInput) {
    res.status(400).json({
      message: 'please input name and email',
    });
  }
  else {
    fs.readFile(PATH_POSTS, "utf-8", (err, user) => {
      let player = JSON.parse(user);
      const playerToAdd = {
        id: player[player.length - 1].id + 1,
        name: nameInput,
        email: emailInput
      }
      player.push(playerToAdd);
      fs.writeFile(PATH_POSTS, JSON.stringify(player), (err) => {
      });
    });
    res.redirect("/game")
  }
})


app.listen(port, () => {
  console.log(`dah nyala nih di ${port}`)
})

